const axios = require("axios");

const update = async () => {
  // Hace la petición al servidor
  const respuesta = await axios.patch(
    "http://127.0.0.1:3000/tareas/627e82041e19c0689b011399",
    {
      titulo: "send package",
      terminada: true,
    }
  );
  // Muestra la respuesta
  console.log(respuesta.status);
  console.log(respuesta.data);
};

update();
