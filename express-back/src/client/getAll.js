const axios = require("axios");

const getAll = async () => {
  // Hace la petición al servidor
  const respuesta = await axios.get("http://127.0.0.1:3000/tareas");
  // Muestra la respuesta
  console.log(respuesta.status);
  console.log(respuesta.data);
};

getAll();
