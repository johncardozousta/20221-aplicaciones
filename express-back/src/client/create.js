const axios = require("axios");

const create = async () => {
  // Hace la petición al servidor
  const respuesta = await axios.post("http://127.0.0.1:3000/tareas", {
    titulo: "Enviar paquete",
  });
  // Muestra la respuesta
  console.log(respuesta.status);
  console.log(respuesta.data);
};

create();
