const express = require("express");

// crea el router
const router = express.Router();

// Ruta raiz
router.get("/", (req, res) => {
  res.send({
    mensaje: "API de Tareas",
  });
});

module.exports = router;
