// Importa los paquetes
const express = require("express");

// Importa la clase
const Tarea = require("../models/Tarea");

// Crea el router
const router = express.Router();

// Datos
const tareas = [
  { id: 1, titulo: "correr" },
  { id: 2, titulo: "leer" },
  { id: 3, titulo: "estudiar" },
];

// Obtiene todas las tareas
router.get("/", async (req, res) => {
  try {
    // Obtiene todas las tareas
    const tareas = await Tarea.find();

    // Retorna las tareas al cliente
    res.status(200).send(tareas);
  } catch (error) {
    console.log(error);
    res.status(500).send({ mensaje: "Error de servidor" });
  }
});

// Obtiene una tarea
router.get("/:id", async (req, res) => {
  // Obtiene el parametro de la URL
  const id = req.params.id;

  try {
    // Obtiene la tarea de la BD
    const tarea = await Tarea.findById(id);

    if (tarea !== null) {
      // Retorna la tarea
      res.status(200).send(tarea);
    } else {
      // Retorna eror 404
      res.status(404).send({
        mensaje: "No se encontró la tarea",
      });
    }
  } catch (error) {
    console.log(error);
    res.status(500).send({ mensaje: "Error de servidor" });
  }
});

// Crea una tarea
router.post("/", async (req, res) => {
  // Obtiene el body
  const body = req.body;

  try {
    // Crea el documento
    const tarea = new Tarea({ titulo: body.titulo });

    // Guarda el documento en la BD
    const resultado = await tarea.save();

    // Responde al cliente
    res.status(201).send(resultado);
  } catch (error) {
    console.log(error);
    res.status(500).send({ mensaje: "Error de servidor" });
  }
});

// Modifica una tarea
router.patch("/:id", async (req, res) => {
  // Obtiene el id de la tarea a modificar
  const id = req.params.id;
  // Obtiene el body
  const body = req.body;
  try {
    // Obtiene el documento y lo actualiza
    const tarea = await Tarea.findOneAndUpdate({ _id: id }, body, {
      new: true,
    });

    // Verifica si modificó una tarea
    if (tarea == null) {
      return res.status(404).send({ mensaje: "No se encontró la tarea" });
    } else {
      // Retorna el documento modificado
      return res.status(200).send(tarea);
    }
  } catch (error) {
    console.log(error);
    res.status(500).send({ mensaje: "Error de servidor" });
  }
});

// Elimina una tarea
router.delete("/:id", async (req, res) => {
  // Obtiene el id
  const id = req.params.id;

  try {
    // Elimina el documento
    const tarea = await Tarea.deleteOne({ _id: id });

    // Retorna el documento eliminado
    res.status(200).send(tarea);
  } catch (error) {
    console.log(error);
    res.status(500).send({ mensaje: "Error de servidor" });
  }
});

// Exporta el router
module.exports = router;
