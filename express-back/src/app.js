// Importar paquetes
require("dotenv").config();
const express = require("express");
const cors = require("cors");

// Importar conexión a BD
const { conexionBD } = require("./db/conexion");

// Crea la aplicación
const app = express();

// Importar rutas
const tareasRoute = require("./routes/tareas");
const inicioRoute = require("./routes/inicio");

// ---- MIDDLEWARE ----
// Habilita el body en el router
app.use(express.json());
app.use(cors());
// Rutas
app.use("/", inicioRoute);
app.use("/tareas", tareasRoute);

// Conexión a MongoDB
conexionBD();

// Inicia el servidor
app.listen(3000);
