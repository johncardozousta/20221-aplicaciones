document.addEventListener("DOMContentLoaded", () => {
  const { ipcRenderer } = require("electron");

  ipcRenderer.on("mostrar-tarea", (evento, datos) => {
    // Obtiene la nueva tarea
    const tarea = datos.tarea;

    // Obtiene la lista
    const listaTareas = document.getElementById("listaTareas");

    // Crea un <li>
    const li = document.createElement("li");
    li.textContent = tarea;

    // Agrega el <li> al <ul>
    listaTareas.appendChild(li);
  });
});
