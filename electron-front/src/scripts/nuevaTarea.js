const textTarea = document.getElementById("textTarea");
const boton = document.getElementById("boton");

// Ubica el cursor el el text
textTarea.focus();

boton.addEventListener("click", (evento) => {
  const { ipcRenderer } = require("electron");

  // Evita el envio de informacion al servidor
  evento.preventDefault();

  // Obtiene el texto
  const texto = textTarea.value;

  ipcRenderer.send("nueva-tarea", {
    tarea: texto,
  });
});
