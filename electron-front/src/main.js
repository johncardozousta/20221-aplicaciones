// Librerías
const { app, BrowserWindow, Menu, ipcMain } = require("electron");
const url = require("url");
const path = require("path");

let ventanaPrincipal;
let ventanaNuevaTarea;

function mostrarVentanaNuevaTarea() {
  // Creación de la ventana de una nueva tarea
  ventanaNuevaTarea = new BrowserWindow({
    width: 400,
    height: 250,
    title: "Nueva tarea",
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
    },
  });
  // Carga del archivo nuevaTarea.html en la ventana
  ventanaNuevaTarea.loadURL(
    url.format({
      pathname: path.join(__dirname, "views/nuevaTarea.html"),
      protocol: "file",
      slashes: true,
    })
  );
  // Limpia la memoria al cerrar la ventana
  ventanaNuevaTarea.on("closed", () => {
    ventanaNuevaTarea = null;
  });
}

function mostrarVentanaPrincipal() {
  // Creación de la ventana principal
  ventanaPrincipal = new BrowserWindow({
    title: "Administrador de Tareas",
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
    },
  });

  // Carga del archivo index.html en la ventana
  ventanaPrincipal.loadURL(
    url.format({
      pathname: path.join(__dirname, "views/index.html"),
      protocol: "file",
      slashes: true,
    })
  );
}

// Evento "ready" de la aplicación
app.on("ready", () => {
  // Muestra la ventan principal
  mostrarVentanaPrincipal();

  // Carga el menu de la aplicación
  const menuPrincipal = Menu.buildFromTemplate(templateMenu);
  Menu.setApplicationMenu(menuPrincipal);

  // Escucha el evento closed para cerrar la aplicación
  ventanaPrincipal.on("closed", () => {
    app.quit();
  });

  // Escucha el evento nueva-tarea
  ipcMain.on("nueva-tarea", (evento, datos) => {
    // Envia los datos a la ventana principal
    ventanaPrincipal.webContents.send("mostrar-tarea", datos);

    // Cierra la venta ade nueva tarea
    ventanaNuevaTarea.close();
  });
});

// Establece la tecla de shortcut
const teclaShortcut = process.platform === "darwin" ? "cmd" : "ctrl";

const templateMenu = [
  {
    label: "Juego",
    submenu: [
      {
        label: "Nuevo juego",
        accelerator: `${teclaShortcut}+N`,
        click() {
          // Muestra la ventana de nueva tarea
          mostrarVentanaNuevaTarea();
        },
      },
      {
        label: "Salir",
        accelerator: `${teclaShortcut}+Q`,
        click() {
          app.quit();
        },
      },
    ],
  },
];

// Verifica si el SO es Mac
if (process.platform === "darwin") {
  // Elimina del menu la opción
  // con el nombre de la app
  templateMenu.unshift({
    label: app.getName(),
  });
}

// Verifica que la aplicación
// no esté empaquetada
if (!app.isPackaged) {
  // Agrega una nueva opción al menú
  templateMenu.push({
    label: "DevTools",
    submenu: [
      {
        label: "Mostrar/Ocultar Dev Tools",
        click(item, focusedWindow) {
          focusedWindow.toggleDevTools();
        },
      },
      {
        role: "reload",
      },
    ],
  });
}
